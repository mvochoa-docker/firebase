# Firebase

Docker images with firebase tools

## Supported tags and respective Dockerfile links

- `10`, `10-alpine`, `10.0.1-alpine`, `latest` [(10.0.1-alpine/Dockerfile)](10.0.1-alpine/Dockerfile)

Example `docker-compose.yml`

```yml
version: '3.8'
services:
  emulators:
    image: mvochoa/firebase:latest
    volumes:
      - .:/app
      - cache:/root/.cache/firebase
    ports:
      - 9099:9099
      - 9000:9000
    environments:
      - FIREBASE_TOKEN=[TOKEN DE FIREBASE LOGIN]
    command: sh -c 'firebase emulators:start'

volumes:
  cache:
```